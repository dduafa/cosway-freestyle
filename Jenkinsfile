pipeline {
    agent any

    options {
        timestamps()
        timeout(time: 10, unit: 'MINUTES')
        gitLabConnection('gitlab')
    }

    environment {
        AWS_REGION = 'ap-south-1'
        AWS_ECR = '644435390668.dkr.ecr.ap-south-1.amazonaws.com'
        RUNTIME_ENV_IP = 'dduafa.crabdance.com'
        RUNTIME_ENV_USER = 'ubuntu@$RUNTIME_ENV_IP'
        RUNTIME_PORT = '8081'
        STATIC_IP = '13.233.247.205'
        REPO = 'derek.duafa'
        PRODUCTION_SERVER = 'ubuntu@dduafa.prod.crabdance.com'
    }

    triggers {
        gitlab(triggerOnPush:true, triggerOnMergeRequest:true, branchFilterType: 'All')
    }

    stages {
        stage('Checkout') {
            steps {
                deleteDir()
                checkout scm
            }
        }
        stage('Build') {
            steps {
                sh('docker build -t $REPO .')
            }
        }
        stage('Test') {
            steps {
                sh '''
                docker rm -f cowsay
                docker network create jenkins.net || true
                docker run -d -p 8081:8080 --name=cowsay --network jenkins.net cowsay:latest
                sleep 5s
                curl -fsSLi $RUNTIME_ENV_IP:8081
                '''
            }
        }
        stage('Publish') {
            steps {
                sh('aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin $AWS_ECR')
                sh('docker tag $REPO:latest $AWS_ECR/$REPO:latest')
                sh('docker push $AWS_ECR/$REPO:latest')
            }
        }
        stage('Deploy') {
            steps {
                script {
                    sh('scp -i /var/jenkins_home/production-svr-key.pem -o StrictHostKeyChecking=no docker-compose.yaml $PRODUCTION_SERVER:~/')
                    sh('''
                        ssh -tt -i /var/jenkins_home/production-svr-key.pem -o StrictHostKeyChecking=no $PRODUCTION_SERVER \
                        aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin $AWS_ECR && \
                        sleep 5s && \
                        docker rm -f cowsay && \
                        docker compose pull cowsay && \
                        docker compose up -d --remove-orphans
                        '''
                    )
                }
            }
        }
        stage('E2E test') {
            steps {
                sh(
                    '''
                    sleep 5s
                    curl http://dduafa.prod.crabdance.com:$RUNTIME_PORT
                    sleep 5s
                    '''
                )
            }
        }
    }

    post {
        always {
            sh '''
                docker rm -f cowsay
                docker rmi -f $REPO:latest
                docker network rm -f jenkins.net
                '''
            emailText subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
                Check console output at $BUILD_URL to view the results.''',
                from: 'jenkins',
                to: emailextrecipients([culprits(), upstreamDevelopers(), developers()])
            cleanWs()
        }

        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
    }
}
